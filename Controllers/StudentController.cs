﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaAngular.DAL.Models;
using PruebaAngular.Services;

namespace PruebaAngular.Controllers
{
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly ILogger<StudentController> _logger;
        private IStudentService StudentService;

        public StudentController(ILogger<StudentController> logger, IStudentService studentService)
        {
            _logger = logger;
            this.StudentService = studentService;
        }

        [HttpGet(template:"api/students/getAll")]
        public IEnumerable<Student> GetAll()
        {
            var result = StudentService.GetAllStudents().ToArray();
            return result;
        }

        [HttpDelete(template: "api/students/{id}")]
        public int RemoveStudent(int id)
        { 
            var result = StudentService.Delete(id);
            return result;
        }

        [HttpGet(template: "api/students/{id}")]
        public Student GetStudent(int id)
        {
            var result = StudentService.GetStudent(id);
            return result;
        }
        [HttpPost(template: "api/students")]
        public ActionResult UpdateStudent(Student student)
        {
            try
            {
                StudentService.UpdateStudent(student);
                return Ok();

            }
            catch (Exception e)
            {
                return Problem("There was an error on the server.");
            }
        }

        [HttpPost(template: "api/students/create")]
        public ActionResult CreateStudent(Student student)
        {
            try
            {
                StudentService.Create(student);
                return Ok();

            }
            catch (Exception e)
            {
                return Problem("There was an error on the server.");
            }
        }
    }
}
