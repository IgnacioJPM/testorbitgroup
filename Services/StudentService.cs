﻿using System;
using System.Collections.Generic;
using PruebaAngular.DAL;
using PruebaAngular.DAL.Models;

namespace PruebaAngular.Services
{
    public class StudentService : IStudentService
    {
        private readonly IDataBase _dataBase;
        public StudentService(IDataBase database)
        {
            this._dataBase = database;
        }
        public List<Student> GetAllStudents()
        {
            var result = _dataBase.GetAll<Student>();
            return result;
        }

        public Student GetStudent(int id)
        {
            try
            {
                var result = _dataBase.Get<Student>(id);

                return result;
            }
            catch (Exception e)
            {
                return new Student()
                {
                    Id = -1
                };
            }

        }

        public int Delete(int id)
        {
            var objToRemove = _dataBase.Get<Student>(id);
            if(objToRemove != null)
                _dataBase.Delete(objToRemove);

            return id;
        }

        public void UpdateStudent(Student student)
        {
             _dataBase.Update<Student>(student);
        }

        public void Create(Student student)
        {
            _dataBase.Create<Student>(student);
        }
    }
}