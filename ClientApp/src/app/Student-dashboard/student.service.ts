import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Student } from '../Interfaces/StudentInterface'


const STUDENT_API: string = 'api/students';

@Injectable()
export class StudentService {
  constructor(private http: HttpClient) {
    debugger;
  }

  getStudents() : Observable<Student[]> {
    return this.http
      .get<Student[]>(`${STUDENT_API}/getAll`)
  }

  getStudent(id: number): Observable<Student> {
    return this.http
      .get<Student>(`${STUDENT_API}/${id}`)
  }

  updateStudent(student: Student): Observable<Student> {
    return this.http
      .post<any>(`${STUDENT_API}`,student)
  }

  removeStudent(student: Student) : Observable<number> {
    return this.http
      .delete<number>(`${STUDENT_API}/${student.id}`)
  }
  createStudent(student: Student) : Observable<number> {
    return this.http.post<any>(`${STUDENT_API}/create`, student)
  }

}
