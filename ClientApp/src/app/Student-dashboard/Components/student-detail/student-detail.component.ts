import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Student } from '../../../Interfaces/StudentInterface'

@Component({
  selector: 'student-detail',
  styleUrls: ['student-detail.component.scss'],
  template: `
  <div class="container">
        <div class="form-group">
          First Name:
              <div class="form-control" style="width: 55%">{{ detail.firstName }} </div>
        </div>
        <div class="form-group">
        Last Name:
            <div class="form-control" style="width: 55%">{{ detail.lastName }} </div>
        </div> 
        <div class="form-group">
        User Name:
            <div class="form-control" style="width: 55%">{{ detail.username }} </div>
        </div> 
        <div class="form-group">
        Career:
            <div class="form-control" style="width: 55%">{{ detail.career }} </div>
        </div>
        <div class="form-group">
        Age:
            <div class="form-control" style="width: 55%">{{ detail.age }} </div>
        </div>       

        <button (click)="toggleEdit()">
          Edit
        </button>
        <button (click)="onRemove()">
          Remove
        </button>
        <div>
  `
})
export class StudentDetailComponent {

  @Input()
  detail: Student;

  @Output()
  edit: EventEmitter<Student> = new EventEmitter<Student>();

  @Output()
  remove: EventEmitter<Student> = new EventEmitter<Student>();

  editing: boolean = false;
  
  toggleEdit() {
      this.edit.emit(this.detail);
  }
  onRemove() {
    this.remove.emit(this.detail);
  }
}