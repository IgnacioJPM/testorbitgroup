import { Component, Input, Output, EventEmitter ,OnInit} from '@angular/core';

import { Student } from '../../../Interfaces/StudentInterface';

@Component({
  selector: 'Student-form',
  styleUrls: ['student-form.component.scss'],
  template: `
    <form (ngSubmit)="handleSubmit(form.value, form.valid)" #form="ngForm" novalidate class="form">
    <div class="form-group">
        Id:        
        <input
            type="text"
            name="id"            
            readonly
            [ngModel]="detail?.id"
            class="form-control">
    </div>

    <div class="form-group">
        Name:        
        <input
            type="text"
            name="firstName"
            required
            #FirstName="ngModel"
            [ngModel]="detail?.firstName"
            class="form-control">

            <div *ngIf="FirstName.errors?.required && FirstName.dirty" class="error">
            student first name is required
            </div>
    </div>
    

    <div class="form-group">
        Last name:
        <input
            type="text"
            name="lastName"
            required
            class="form-control"
            #Lastname="ngModel"
            [ngModel]="detail?.lastName">
        <div *ngIf="Lastname.errors?.required && Lastname.dirty" class="error">
            student last name is required
        </div>
    </div>

    <div class="form-group">
        Username:
            <input
                class="form-control"
                type="text"
                name="username"
                #username="ngModel"
                [ngModel]="detail?.username">
    </div>    
    <div class="form-group">
        Career:
            <input
                type="text"
                class="form-control"
                name="career"
                #career="ngModel"
                [ngModel]="detail?.career">
    </div>
    <div class="form-group">
        Age:
            <input
                type="number"
                class="form-control"
                name="age"
                #age="ngModel"
                [ngModel]="detail?.age">
    </div>

      <button *ngIf="detail" type="submit" [disabled]="form.invalid">
        Update student
      </button>
      <button *ngIf="!detail" type="submit" [disabled]="form.invalid">
        Create student
      </button>

    </form>
  `
})
export class StudentFormComponent implements OnInit{

  @Input()
  detail: Student;

  @Output()
  update: EventEmitter<Student> = new EventEmitter<Student>();

  ngOnInit(){
    console.log(this.detail);
  }
  handleSubmit(student: Student, isValid: boolean) {
    if (isValid) {
      console.log(student)
      this.update.emit(student);
    }
  }

}
