import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { switchMap } from 'rxjs/operators';


import { StudentService } from '../student.service';

import { Student } from '../../Interfaces/StudentInterface';

@Component({
  selector: 'Student-viewer',
  template: `
    <div>
      <Student-form
        [detail]="student"
        (update)="onUpdatestudent($event)">
      </Student-form>
    </div>
  `
})
export class StudentViewerComponent implements OnInit {
  student: Student;
  constructor(   
    private route: ActivatedRoute,
    private router: Router,
    private studentService: StudentService
  ) {}
  ngOnInit() {
    console.log(this.route.params)
    this.route.params.pipe(
            switchMap((data: Student) => this.studentService.getStudent(data.id)))
            .subscribe((data: Student) => {
                if (data.id == -1){ //means we didnt find that student
                  this.router.navigate(["/not-found"])
                }else{
                  this.student = data
                }
                });
  }
  onUpdatestudent(event: Student) {
    this.studentService
      .updateStudent(event)
      .subscribe((data: any) => {
        this.router.navigate(["/students"])
      }); 
  }
}