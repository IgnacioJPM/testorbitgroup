export interface Student {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  career: string;
  age: number;
}
