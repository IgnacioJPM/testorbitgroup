import { Component } from '@angular/core';

@Component({
  selector: 'not-found',
  template: `
    <div>
      No se encontro la pagina o el estudiante<a routerLink="/">Volver al inicio</a>?
    </div>
  `
})
export class NotFoundComponent {}