﻿using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace PruebaAngular.DAL
{
    public class Database :IDataBase
    {
        private readonly string dbFileName;

        public Database(DbConfig dbConfig)
        {
            this.dbFileName = dbConfig.DatabaseName;
        }

        public void Update<T>(T value)
        {
            using (var db = new SQLiteConnection(dbFileName))
            {
                 db.Update(value);
            }
        }

        public void Create<T>(T value) 
        {
            using (var db = new SQLiteConnection(dbFileName))
            {
                db.Insert(value);
            }
        }

        public T Get<T>(object identifier) where T : new()
        {
            using (var db = new SQLiteConnection(dbFileName))
            {
                return db.GetWithChildren<T>(identifier);
            }
        }

        public List<T> GetAll<T>() where T : new()
        {
            using (var db = new SQLiteConnection(dbFileName))
            {
                return db.GetAllWithChildren<T>();
            }
        }

        public void Delete<T>(T objToRemove)
        {
            using (var db = new SQLiteConnection(dbFileName))
            {
                db.Delete(objToRemove);
            }
        }
    }
}