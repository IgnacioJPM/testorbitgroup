﻿using System.Collections.Generic;
using PruebaAngular.DAL.Models;

namespace PruebaAngular.DAL
{
    public interface IDataBase
    {
        T Get<T>(object identifier) where T : new();
        List<T> GetAll<T>() where T : new();
        void Update<T>(T value);
        void Create<T>(T value) ;

        void Delete<T>(T objToRemove);
    }
}