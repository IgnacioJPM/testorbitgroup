﻿using SQLite;

namespace PruebaAngular.DAL.Models
{
    public class Student
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Career { get; set; }

    }
}